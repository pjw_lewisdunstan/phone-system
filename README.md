# PJ Web Package Framework
This package allows the standard files and directories to be set up for a new package.

## Installing the framework
To create a new package from the framework, run the following command from your `c:/work` directory:
```
composer create-project pjwebsolutions/package-framework ./[PACKAGE NAME] "[VERSION]" --repository-url=https://repo.packagist.com/pjwebsolutions
```
Replace `[PACKAGE NAME]` with the name of the new package, and `[VERSION]` with the version of the framework you wish to use.

To create a new package called `new-awesome-package` that uses version `1`, the below command would be used:
```
composer create-project pjwebsolutions/package-framework ./new-awesome-package "1.0.0" --repository-url=https://repo.packagist.com/pjwebsolutions
```

Composer will clone the repository into the directory and will run `composer update` to pull in all of the dependencies.

## The Included Files and Directories
The included files and directories are mostly self explanatory.
| File/Directory      | Description                                                                                                   |
| ------------------- | ------------------------------------------------------------------------------------------------------------- |
| `assets`            | Stores any assets that a project may use, such as views, email views, SCSS, and JS                            |
| `config`            | Stores the base config file if used by the package.                                                           |
| `db`                | Stores any migrations or seeds needed by the package.                                                         |
| `src`               | Stores the classes for the package. Note that the Directory structure **MUST** match the namespace structure. |
| `tests`             | Stores PHPUnit tests                                                                                          |
| `CHANGELOG.md`      | Stores the changelog history, this will normally be updated by the PO                                         |
| `composer.json`     | Stores the packages name and any dependencies, as well as autoloading instructions.                           |
| `phpstan.neon.dist` | Stores the configuration for PHPStan.                                                                         |
| `phpunit.xml`       | Stores the configuration for PHPUnit.                                                                         |

## Next Steps
There are a few things that need to be done before you can start working on the package.

### Updating Framework Files
1. In `composer.json` update the name property to the name of the package. Following the example we used above, this would be `pjwebsolutions/new-awesome-package`.

### Getting the Project Ready For Development
1. Commit the work to the master branch. (This needs to happen before you create a develop or story branch)
2. Go to Private Packagist and make sure the project has been found and picked up.
3. Run `git flow init` in the packages folder, follow the instructions.
4. Create your story branch and switch to it.
5. Happy deving!
